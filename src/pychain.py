
def C(collection):
	return PyChain(collection)

class PyChain:
	
	collection = []
	def __init__(self,collection):
		self.collection = collection
		pass

	def to_list(self,):
		return self.collection

	def filter(self,function):
		a = [obj for obj  in self.collection if function(obj)]  
		return PyChain(a)
	
	def filter_not(self,function):
		a = [obj for obj  in self.collection if not function(obj)]  
		return PyChain(a)

	def foreach(self,function):
		for c in self.collection:
			function(c)
	
	def map(self,function):
		a = [function(obj) for obj  in self.collection]  
		return PyChain(a)



