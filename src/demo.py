from pychain import *
def f(s):
	print(s)


#filter
print(C([2,3,1,6,7,9]).filter(lambda x:x<=5)).to_list()
print(C([2,3,1,6,7,9]).filter_not(lambda x:x<=5)).to_list()
#map
print(C(["A","B","C"]).map(lambda x:x.lower())).to_list()
#or with function
def lower(a):
	return a.lower()
	print(C(["A","B","C"]).map(lower)).to_list()

#foreach
def print_it(a):
	print(a)

C(["A","B","C"]).foreach(print_it)




